package nl.patrickdevelopment.mysqlapidemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class InsertActivity extends AppCompatActivity {

    EditText ETvoornaam,ETachternaam,ETemail;
    Button BtnSave;
    RequestQueue requestQueue;
    final String INSERT_URL = "http://192.168.56.1/android_api/create_user.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        ETachternaam = (EditText) findViewById(R.id.ETachternaam);
        ETvoornaam = (EditText) findViewById(R.id.ETvoornaam);
        ETemail = (EditText) findViewById(R.id.ETemail);
        BtnSave = (Button) findViewById(R.id.BtnSend);

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        BtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringRequest postUsers = new StringRequest(
                        Request.Method.POST,
                        INSERT_URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("post user response", response.toString());
                                finish();

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("post user", error.toString());
                            }
                        }
                ){
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String,String>();
                        params.put("voornaam",ETvoornaam.getText().toString());
                        params.put("achternaam",ETachternaam.getText().toString());
                        params.put("email",ETemail.getText().toString());
                        return params;
                    }
                };

                requestQueue.add(postUsers);

            }
        });

    }
}
