package nl.patrickdevelopment.mysqlapidemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    RequestQueue requestQueue;
    Button btnGet, btnInsert;
    TextView tvUsers;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        url = "http://192.168.56.1/android_api/show_user.php";

        btnGet = (Button) findViewById(R.id.button);
        btnInsert = (Button) findViewById(R.id.btnInsert);
        tvUsers = (TextView) findViewById(R.id.tvusers);

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JsonObjectRequest getUsers = new JsonObjectRequest(
                        Request.Method.POST,
                        url,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d("get user response", response.toString());
                                try {
                                    JSONArray users = response.getJSONArray("users");
                                    tvUsers.setText("");

                                    for (int i = 0; i < users.length(); i++) {

                                        JSONObject user = users.getJSONObject(i);

                                        String voornaam = user.getString("voornaam");
                                        String achternaam = user.getString("achternaam");
                                        String email = user.getString("email");
                                        String create_date = user.getString("create_time");

                                        tvUsers.append(voornaam+" \n"+achternaam+" \n"+email+" \n"+create_date+" \n \n");
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("get user", error.toString());
                            }
                        }
                );

                requestQueue.add(getUsers);
            }
        });

        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, InsertActivity.class));
            }
        });

    }
}
